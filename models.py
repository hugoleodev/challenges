from django.db import models


class Teacher(models.Model):
    name = models.Charfield(max_length=10)

    def __unicode__(self):
        return self.name


class Student(models.Model):
    name = models.Charfield(max_length=10)

    def __unicode__(self)
        return self.name

class Course(models.Model):
    name = models.Charfield()
    student = models.ManyToMany(Student)
    teacher = models.ForeignKey(Teacher)

    def __unicode__(self):
        return self.name

teacher = Teacher.objects.first()
teacher.course_set.all()

student = Student.objects.first()
student.course_set.all()


